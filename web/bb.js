var bbProfession = function (code, caption, roles) {
    this.code = code;
    this.caption = caption;
    this.roles = roles;
};

var bbProfessionRole = function(code, caption, salary, age) {
    this.code = code;
    this.caption = caption;
    this.salary = salary;
    this.age = age;
};

var bbCalculator = function (data, rpi, plusRpi, percentContribution, annualThreshold) {
    this.data = data;
    this.rpi = rpi;
    this.plusRpi = plusRpi;
    this.percentContribution = percentContribution;
    this.annualThreshold = annualThreshold;
    this.interestRate = this.rpi + this.plusRpi;
    this.dataObj = [];
    this.dataError = false;
    this.errorMessage = null;
    this.init = function () {
        if (typeof this.data === 'object' && this.data.length > 0) {
            var profRoles = [];
            for (var profCounter in this.data) {
                if (typeof this.data[profCounter].roles === 'object' && this.data[profCounter].roles.length > 0) {
                    for (var roleCounter in this.data[profCounter].roles) {
                        var profRole = new bbProfessionRole(
                            this.data[profCounter].roles[roleCounter].code,
                            this.data[profCounter].roles[roleCounter].caption,
                            this.data[profCounter].roles[roleCounter].salary,
                            this.data[profCounter].roles[roleCounter].age
                        );
                        this.addSelectOption("experienceLevel", profRole.code, profRole.caption);
                        profRoles.push(profRole);
                    }
                }
                var profession = new bbProfession(this.data[profCounter].code, this.data[profCounter].caption, profRoles);
                this.dataObj.push(profession);
                this.addSelectOption("profession", profession.code, profession.caption);
            }
        }

        //Populate age select options
        var ageCounter = 14;
        while (ageCounter <=50) {
            this.addSelectOption("age", ageCounter, ageCounter);
            ++ageCounter;
        }
    };
    this.addSelectOption = function (id, key, value) {
        $('#'+id).append($("<option/>", {
            value: key,
            text: value
        }));
    };
    this.process = function () {
        var maintenanceLoan = 0;
        var studentLoan = 0;
        if (this.isValidAmount($('#maintenanceLoan').val())) {
            maintenanceLoan = parseInt($('#maintenanceLoan').val());
        }
        if (this.isValidAmount($('#studentLoan').val())) {
            studentLoan = parseInt($('#studentLoan').val());
        }
        var totalLoan = maintenanceLoan + studentLoan;
        if (totalLoan > 0) {
            totalLoan += this.calculateInterest(totalLoan, this.interestRate);
            $('#loanCalculatedData1').html("&pound;" + totalLoan);
            var profession = this.getCurrentDataObj($('#profession').val());
            if (typeof profession !== 'undefined') {
                var profRole = this.getCurrentProfRole($('#experienceLevel').val(), profession);
                if (typeof profRole !== 'undefined') {
                    $('#loanCalculatedData2').html("&pound;" + profRole.salary);
                }
            }
        }
    };
    this.isValidAmount = function (amount) {
        if (!isNaN(amount) && amount > 0) {
            return true;
        }
        return false;
    };
    this.calculateInterest = function (principal, rate) {
        return (Math.round((principal * rate)/100));
    };
    this.getCurrentDataObj = function (currentProfessionCode) {
        if (this.dataObj.length > 0) {
            for (var profCounter in this.dataObj) {
                if (this.dataObj[profCounter].code == currentProfessionCode) {
                    return this.dataObj[profCounter];
                }
            }
        }
    };
    this.getCurrentProfRole = function (profRoleCode, profession) {
        if (typeof profession === 'object' && profession.roles.length > 0) {
            for (var roleCounter in profession.roles) {
                if (profession.roles[roleCounter].code == profRoleCode) {
                    return profession.roles[roleCounter];
                }
            }
        }
    };
    this.calculateRepayment = function () {

    };
};